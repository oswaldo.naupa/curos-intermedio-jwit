import { styled } from 'goober';

const Container = styled('button')`
    border-radius: 10px;
    background-color: #d53d3c;
    width: 100%;
    border: 1px solid #d53d3c;
    width: 100%;
    height: 45px;
    color: white;
    font-size: 18px;
`

export interface params {
    label: string,
    className?: string,
    onClick?: () => void
}

const App = (params: params):JSX.Element => {

  return (
    <Container className={ params.className } onClick={params.onClick}>
        { params.label }
    </Container>
  )
}

App.defaultProps = {
  className: "button"
}

export default App
