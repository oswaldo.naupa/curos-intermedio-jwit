import { styled } from 'goober';
import Button from './button';
import MiniCard from './miniCard';

const Container = styled('div')<{ image: string }>`
    position: relative;
   
    height: 100vh;
    width: 100vw;
    
    overflow: hidden;
    .mask{
        position: absolute;
        inset: 0;
        background-color: #0a0a0a4b;
        
    }

    .body {
        height: 60vh;
        background-color: #111113;
        padding: 15px;
        display: flex;
        flex-direction: column;
        gap: 20px;
       
    }

    .title{
        color: white;
        margin: 0;
        font-size: 38px;
    }

    .description{
        color: #cfced1;
        margin: 0;
    }

    .swipe{
        display: flex;
        gap: 15px;
    }

    .footer{
        display: flex;
       align-items: center;
       gap: 15px;
    }

    .header{
        position: relative;
        width: 100vw;
        height: 40vh;
        background-image: url(${props => props.image});
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;

        .cover{
            position: absolute;
            bottom: 10px;
            width: 100%;
            display: flex;
            flex-direction: row;
            justify-content: space-around;

            .stat{
                color: white;
            }
        }
    }
`

export interface params {
    image: string,
    onClick?: () => void
}

const App = (params: params):JSX.Element => {

  return (
    <Container image={params.image}>

        <div className='header'>
            <div className="mask"></div> 
            <div className='cover'>
                <h2 className='title'>Costa Coffee</h2>
                <p className='stat'>4.5</p>
            </div>
            
        </div>
        

        <div className="body">
           
            <p className='description'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel, placeat, libero ullam similique recusandae dolores alias aspernatur dicta provident veritatis, nesciunt reiciendis? In iusto et ut ea labore. Vel, in?</p>

            <div className='footer'>
                <p className='description'>4 personas</p>
                <p className='description'>Envio a domicilio</p>
            </div>

            <Button label='Solicitar pedido'/>

            <div className='swipe'>
                <MiniCard image='./2.jpg' title='Cafe' description='description' onClick={params.onClick}/>
                <MiniCard image='./3.jpg'  title='Pasteles' description='15%' onClick={params.onClick}/>
                
            </div>
           
        </div>
    </Container>
  )
}

export default App

