import { styled } from 'goober';
import Button from '../components/button';
import {useState} from 'react';

const Container = styled('div')<{ image: string, visible: boolean }>`
        position: fixed;
        inset: 0;
        z-index: 100;
        width: 100vw;
        height: 100vh;
        background-image: url(${props => props.image});
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;

        transform: ${props => props.visible ? 'translateX(0%)' : 'translateX(-100%)'};

        transition: all .4s ease-in-out;
        .card{
            position: absolute;
            background-color: #0A0A0Ae6;
            width: 100%;
            bottom: 0;
            border-radius: 7% 7% 0px 10px;
            padding: 20px;
            backdrop-filter: blur(4px);

            display: flex;
            flex-direction: column;
            gap: 15px;
        }

        .title{
        color: white;
        margin: 0;
        font-size: 38px;
    }

    .description{
        color: #cfced1;
        margin: 0;
    }

    .button{
        
        background-color: red;
        width: 65%;
    }

    .btn-back{
        background-color: white;
        border: 1px solid white;
        color: black;
        width: 30%;
    }

    .row{
        display: flex;
        justify-content: space-between;
        align-items: center;
        gap: 15px;
    }

    .counter{
        color: white;
        font-size: 28px;
    }

    .btn-operator{
       background-color: #303030;
       border: 1px solid #303030;
       width: 45px;
    }

    .price{
        color: white;
        font-size: 30px;
    }
`

export interface params {
    visible: boolean,
    price: number,
    image: string,
    title: string,
    description?: string,
    onClick?: () => void,
}

const App = (params: params):JSX.Element => {

    const [counter, setCounter] = useState<number>(0)

    const handleAdd = () => setCounter(counter + params.price)
    const handleSub = () =>counter - params.price >= 0 &&  setCounter(counter - params.price)

  return (
    <Container image={params.image} visible={params.visible}>
        <div className="card">
            <h2 className='title'>{ params.title }</h2>
            <p className='description'>{ params.description }</p>

            <div className='row'>
                <div className='row'>
                    <Button className='btn-operator' label='-' onClick={handleSub}/>
                    <p className='counter'>{counter/params.price}</p>
                    <Button className='btn-operator' label='+' onClick={handleAdd}/>
                </div>
                <p className='price'>$ { counter }</p>
            </div>

            <div className='row'>
                <Button label='Pagar'/>
                <Button className='btn-back' label='Volver' onClick={params.onClick}/>
            </div>
            
        </div>
    </Container>    
  )
}

export default App