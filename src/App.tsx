import { styled } from 'goober';
import Card from './components';
import Detalle from './detalle';
import { useState } from 'react';

const Container = styled('div')`
  background-color: #252628;
  height: 100vh;

  display: flex;
  flex-direction: column;
  gap: 15px;
  --card-background-color: #2e2f30;
  --card-border: 1px solid #343536;
  --card-selected: #0097d945;
  --font-family: "Gill Sans", "Gill Sans MT", Calibri, 'Trebuchet M5', sans-serif;

  *{
    font-family: var(--font-family);
    box-sizing: border-box;
  }
`

const App = ():JSX.Element => {

  const [visible, setVisible] = useState<boolean>(false)

  return (
    <Container>
        <Card image='/1.jpg' onClick={() => setVisible(true)}/>
        <Detalle image='/3.jpg' title='Costa Coffee' description='Dolores alias aspernatur dicta provident veritatis, nesciunt reiciendis? In iusto et ut ea labore. Vel, in?' visible={visible} price={45000}
         onClick={() => setVisible(false)}/>
    </Container>
  )
}

export default App
